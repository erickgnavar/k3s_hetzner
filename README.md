k3s Hetzner
===========

How to build a kubernetes cluster in [Hetzner Cloud](https://www.hetzner.com/cloud).

## Why

Hetzner is a good cloud provider in Europe, this project show us how build a kubernetes cluster in Hetzner Cloud, using [k3s](https://k3s.io/), [cloud-init](https://cloudinit.readthedocs.io/en/latest/), [Terraform](https://www.terraform.io/) and [Ansible](https://docs.ansible.com/ansible/latest/index.html).

## Requirements.

- A Hetzner Cloud account.

## Packages used by the build process

- Cloud Init.
- Terraform 0.12.
- Ansible 2.8.

we get k3s 1.0.1 installed.
